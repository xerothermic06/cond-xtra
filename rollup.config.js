import babel from 'rollup-plugin-babel';
import {uglify} from 'rollup-plugin-uglify';

export default [{
    //output for CJS version
    input: 'cond.js',
    plugins: [
        babel({presets: ['@babel/preset-env']})
    ],
    context: 'null',
    moduleContext: 'null',
    output: {
        exports: 'default',
        file: 'index.js',
        format: 'cjs',
        name: 'cond'
    }
},
{
    //output for minified version
    input: 'cond.js',
    plugins: [
        babel({presets: ['@babel/preset-env']}),
        uglify()
    ],
    context: 'null',
    moduleContext: 'null',
    output: {
        exports: 'default',
        file: 'cond.min.js',
        format: 'iife',
        name: 'cond'
    }
},];
