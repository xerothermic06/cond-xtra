
const tressa = require('tressa');

const cond = require('../index');

const listsEqual = (l1, l2) => l1.every((n, i) => n === l2[i]) && l2.every((n, i) => n === l1[i]);

tressa.log('# testing cond expressions');
(async () => {
    const x = 1 + 2 + 8,
        // closures may be inner or outer
        branchesLazy = [
            [() => x & 1, () => 'mod one'],
            [() => x & 2, () => 'mod two'],
            () => [x & 4, 'mod four'],
            () => [x & 8, 'mod eight'],
            null
        ],
        branchesMixed = [
            [() => x & 1, 'mod one'],
            [x & 2, () => 'mod two'],
            [() => x & 4, () => 'mod four'],
            () => [x & 8, 'mod eight'],
            null
        ],
        branchesFullEval = [
            [x & 1, 'mod one'],
            [x & 2, 'mod two'],
            [x & 4, 'mod four'],
            [x & 8, 'mod eight'],
            null
        ],
        branchesFuncValues = [
            [x & 1, cond.value(() => 1)],
            [x & 4, cond.value(() => 4)],
            null
        ],
        noDefault = [
            [x & 16, 'mod sixteen'],
            [x & 32, 'mod thirty two']
        ],
        multiNoDefault = [
            [x & 1, 'mod one'],
            [x & 2, 'mod two'],
        ],
        invalidClauses = [
            1,
            new Set([1,2,3]),
            [x & 1, 'mod one'],
            null
        ],
        parameterized = [ // Example taken from miguelmota's cond module
            [t => t === 0, 'Water Freezes at 0C'],
            [t => t === 100, 'Water boils at 100C'],
            t => `Nothing special happens at ${t || '?'}C`
        ];
    const val1 = cond(...branchesLazy),
        val2 = cond(...branchesMixed),
        val3 = cond(...branchesFullEval);

    tressa.log('## single cond');
    tressa(val1 === 'mod one', 'cond single evaluates correctly');
    tressa(val1 === val2 && val2 === val3 && val1 === val3, 'lazy and immediate versions of cond return same values');

    const mm1 = cond.multi(...branchesLazy),
        mm2 = cond.multi(...branchesMixed),
        mm3 = cond.multi(...branchesFullEval),
        multiShouldEqual = ['mod one', 'mod two', 'mod eight'];

    tressa.log('## multi cond');
    tressa(listsEqual(mm1, multiShouldEqual)
        && listsEqual(mm2, multiShouldEqual)
        && listsEqual(mm3, multiShouldEqual),
    'multi cond evaluates correctly');

    const funcValues = cond.multi(...branchesFuncValues);
    tressa(funcValues.reduce((p, f) => p += f(), 0) === 1, 'wrapping function values works');

    tressa.log('## null default');

    const shouldBeNull = cond.nd(...noDefault);
    tressa(shouldBeNull === null, 'null default works');

    const shouldHaveTwoEls = cond.multind(...multiNoDefault);
    tressa(shouldHaveTwoEls.length === 2, 'multi null default works');

    tressa.log('## test singleton');
    const shouldBeOne = cond(1);
    tressa(shouldBeOne === 1, 'singleton cond works');

    tressa.log('## test parameterized');
    const paramCond = cond.func(parameterized);
    tressa.log(paramCond(0));
    tressa(paramCond(0) === 'Water Freezes at 0C'
        && paramCond(100) === 'Water boils at 100C'
        && paramCond(42) === 'Nothing special happens at 42C',
    'parameterized cond works');

    tressa.log('## test errors');
    tressa.async(done => {
        try {
            cond(...invalidClauses);
        } catch (e) {
            tressa(e.message === 'cond clauses must be Arrays',
                'fails correctly when given invalid clauses');
            done();
        }
    }, 500);
})();
