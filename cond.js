const prdctFalse = Symbol('prdctFalse'),
    returnValue = Symbol('returnValue');

const unwrap = (value) => {
    return (value === null || value === undefined)
        ? value
        : value[returnValue]
            ? value.value
            : value instanceof Function
                ? value()
                : value;
};

const takePath = (truthExpr, result) => {
    const predTrue = (truthExpr instanceof Function)
        ? truthExpr()
        : truthExpr;
    if (predTrue) {
        return unwrap(result);
    } else return prdctFalse;
};

const condExpr = (clauseList, returnFirst) => {
    let matches = null;
    for (const c of clauseList.slice(0, -1)) {
        const clause = c instanceof Function ? c() : c;
        if (!Array.isArray(clause)) {
            throw Error('cond clauses must be Arrays');
        }
        const result = takePath(...clause);
        if (result !== prdctFalse) {
            if (returnFirst) { return result; }
            if (!matches) { matches = []; }
            matches.push(result);
        }
    }
    if (!matches) {
        return unwrap(...clauseList.slice(-1));
    }
    return matches;
};

/** @typedef {[Function|any, Function|any]} CondClause */

/**
 * Iterate through a series of clauses until one of them has a true predicate.
 * Predicates and return values can both be closures in order to evaluate them
 * lazily. If it's necessary to return a function as a value, the cond.value
 * function can be used  to wrap it, or it can be returned from another
 * anonymous function. If none of the clauses has a true predicate, return the
 * last clause as the default.
 * @param  {...CondClause} clauses
 */
const cond = (...clauses) => {
    return condExpr(clauses, true);
};

/**
 * Returns an Array containing the return values for all clauses with true
 * predicates. Same lazy evaluation rule holds from regular cond.
 * @param  {...CondClause} clauses
 */
cond.multi = (...clauses) => {
    return condExpr(clauses);
};

/**
 * Wraps a return value so it isn't accidentally evaluated when its predicate is
 * true. This should be used for any return value for which `x instanceof Function`
 * is true. This can wrap any value, but is only useful for callable
 * return values.
 */
cond.value = (value) => {
    return {[returnValue]: true, value};
};

/**
 * Same as cond, but automatically provides *n*ull as the *d*efault value.
 * @param  {...CondClause} clauses
 */
cond.nd = (...clauses) => {
    return cond(...[...clauses, null]);
};

/**
 * Same as cond.multi, but automatically provides null as the default value.
 * @param  {...CondClause} clauses
 */
cond.multind = function(...clauses) {
    return this.multi(...[...clauses, null]);
};
/**
 * Generate a parameterized `cond` construct in which the predicates are
 * functions with arguments.
 * @param  {...CondClause} clauses
 * @returns [Function]
 */
cond.func = function(clauses) {
    return (...args) =>
        cond(...clauses.map((clause) =>
            Array.isArray(clause)
                ? clause[0] instanceof Function
                    ? [() => clause[0](...args), clause[1]]
                    : clause
                : clause instanceof Function
                    ? () => clause(...args)
                    : clause));
};

export default cond;
