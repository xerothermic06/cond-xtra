'use strict';

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  }
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

var prdctFalse = Symbol('prdctFalse'),
    returnValue = Symbol('returnValue');

var unwrap = function unwrap(value) {
  return value === null || value === undefined ? value : value[returnValue] ? value.value : value instanceof Function ? value() : value;
};

var takePath = function takePath(truthExpr, result) {
  var predTrue = truthExpr instanceof Function ? truthExpr() : truthExpr;

  if (predTrue) {
    return unwrap(result);
  } else return prdctFalse;
};

var condExpr = function condExpr(clauseList, returnFirst) {
  var matches = null;
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = clauseList.slice(0, -1)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var c = _step.value;
      var clause = c instanceof Function ? c() : c;

      if (!Array.isArray(clause)) {
        throw Error('cond clauses must be Arrays');
      }

      var result = takePath.apply(void 0, _toConsumableArray(clause));

      if (result !== prdctFalse) {
        if (returnFirst) {
          return result;
        }

        if (!matches) {
          matches = [];
        }

        matches.push(result);
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  if (!matches) {
    return unwrap.apply(void 0, _toConsumableArray(clauseList.slice(-1)));
  }

  return matches;
};
/** @typedef {[Function|any, Function|any]} CondClause */

/**
 * Iterate through a series of clauses until one of them has a true predicate.
 * Predicates and return values can both be closures in order to evaluate them
 * lazily. If it's necessary to return a function as a value, the cond.value
 * function can be used  to wrap it, or it can be returned from another
 * anonymous function. If none of the clauses has a true predicate, return the
 * last clause as the default.
 * @param  {...CondClause} clauses
 */


var cond = function cond() {
  for (var _len = arguments.length, clauses = new Array(_len), _key = 0; _key < _len; _key++) {
    clauses[_key] = arguments[_key];
  }

  return condExpr(clauses, true);
};
/**
 * Returns an Array containing the return values for all clauses with true
 * predicates. Same lazy evaluation rule holds from regular cond.
 * @param  {...CondClause} clauses
 */


cond.multi = function () {
  for (var _len2 = arguments.length, clauses = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    clauses[_key2] = arguments[_key2];
  }

  return condExpr(clauses);
};
/**
 * Wraps a return value so it isn't accidentally evaluated when its predicate is
 * true. This should be used for any return value for which `x instanceof Function`
 * is true. This can wrap any value, but is only useful for callable
 * return values.
 */


cond.value = function (value) {
  var _ref;

  return _ref = {}, _defineProperty(_ref, returnValue, true), _defineProperty(_ref, "value", value), _ref;
};
/**
 * Same as cond, but automatically provides *n*ull as the *d*efault value.
 * @param  {...CondClause} clauses
 */


cond.nd = function () {
  for (var _len3 = arguments.length, clauses = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
    clauses[_key3] = arguments[_key3];
  }

  return cond.apply(void 0, [].concat(clauses, [null]));
};
/**
 * Same as cond.multi, but automatically provides null as the default value.
 * @param  {...CondClause} clauses
 */


cond.multind = function () {
  for (var _len4 = arguments.length, clauses = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
    clauses[_key4] = arguments[_key4];
  }

  return this.multi.apply(this, [].concat(clauses, [null]));
};
/**
 *
 */


cond.func = function (clauses) {
  return function () {
    for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
      args[_key5] = arguments[_key5];
    }

    return cond.apply(void 0, _toConsumableArray(clauses.map(function (clause) {
      return Array.isArray(clause) ? clause[0] instanceof Function ? [function () {
        return clause[0].apply(clause, args);
      }, clause[1]] : clause : clause instanceof Function ? function () {
        return clause.apply(void 0, args);
      } : clause;
    })));
  };
};

module.exports = cond;
